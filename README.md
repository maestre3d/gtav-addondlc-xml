# Grand Theft Auto V's DLCList generator

Light Minimalist app that helps you with DLCList.xml generation.

This app remembers your GTA V's path once you launch it for the first time.
It automatically detects if your game is modded.
No update needed when GTA V update comes out.

The app contains a little viewport so you'll be able to see your current mods.
Also, a little box to see your current path.
( If you already configured it, path will load automatically :) )

So, after first run you'll just need to click two buttons and your DLCList is ready to go.

Added Light & Dark Mode Themes.

*40Mb max. consumption

Programmed in C# .NET.

Used MVC pattern w WinForms.

{ DevDependencies: 

    "Newtonsoft JSON",

    "WinAPICodePack",

    "MetroModernUI",

    "XML"
    
}

{ FontDependencies:

    "FontsAwesome Free 5",

    "Segoe UI",

    "AvenirNext LT Pro"

}
