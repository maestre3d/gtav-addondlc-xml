﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controllers;
using System.IO;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace AddonX
{
    public partial class root_menu : Form
    {
        private bool win_state = false;
        private bool color_state = false;
        private bool new_path = false;
        private bool mod_flag = false;
        private bool mod=false;
        string x;
        private Point lastPoint;
        pathController c = new pathController();
        modsController m = new modsController();

        public root_menu()
        {
            InitializeComponent();

            openBtn.Controls.Add(icon_folder);
            icon_folder.Location = new Point(10, 11);
            icon_folder.BackColor = Color.Transparent;
            saveBtn.Controls.Add(icon_save);
            icon_save.Location = new Point(10, 11);
            icon_save.BackColor = Color.Transparent;

            
            x = c.getPathJSON();
            if ( x != null )
            {
                txb_path.Text = x;
            }
            
        }


        private void exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void win_max_Click(object sender, EventArgs e)
        {
            // If Win state = normal
            if ( win_state == false )
            {
                this.WindowState = FormWindowState.Maximized;
                win_state = true;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                win_state = false;
            }
        }

        private void win_min_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void win_panel_MouseMove(object sender, MouseEventArgs e)
        {
            if ( e.Button == MouseButtons.Left )
            {
                this.Left += e.X - lastPoint.X;
                this.Top += e.Y - lastPoint.Y;
            }
        }

        private void win_panel_MouseDown(object sender, MouseEventArgs e)
        {
            lastPoint = new Point(e.X, e.Y);
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if ( x == null || new_path == true )
            {
                string message = null;
                c.savePath(ref message);
                MessageBox.Show(message, "File Creation", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                MessageBox.Show("Path is already loaded", "Path Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void openBtn_Click(object sender, EventArgs e)
        {
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = "C:\\Users";
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                //MessageBox.Show("Path selected: " + dialog.FileName, "Folder Selected");
                new_path = true;
                c.setPath(dialog.FileName);
                txb_path.Text = dialog.FileName;
            }
        }

        private void openBtn_MouseHover(object sender, EventArgs e)
        {
            metroToolTip1.SetToolTip(openBtn,"Select your GTA V's Directory");
            
        }

        private void saveBtn_MouseHover(object sender, EventArgs e)
        {
            metroToolTip1.SetToolTip(saveBtn, "Save your GTA V's current Directory");
        }

        // Color modes
        private void clrBtn_Click(object sender, EventArgs e)
        {
            // False = Dark Mode
            if (color_state != true)
            {
                // Light Mode
                color_state = true;
                this.clrBtn.Text = "Dark Mode";
                lbl_path.BackColor = Color.White;
                lbl_path.ForeColor = Color.FromArgb(20, 20, 20);
                txb_path.BackColor = Color.White;
                txb_path.ForeColor = Color.FromArgb(20, 20, 20);
                lbl_userm.BackColor = Color.White;
                lbl_userm.ForeColor = Color.FromArgb(20, 20, 20);
                txb_list.BackColor = Color.White;
                txb_list.ForeColor = Color.FromArgb(20, 20, 20);
                main_panel.BackColor = Color.White;
                mods_toggle.Theme = MetroFramework.MetroThemeStyle.Light;
                lbl_mod.BackColor = Color.White;
                lbl_mod.ForeColor = Color.FromArgb(20, 20, 20);
            }
            else
            {
                // Dark Mode
                color_state = false;
                this.clrBtn.Text = "Light Mode";
                lbl_path.BackColor = Color.FromArgb(20, 20, 20);
                lbl_path.ForeColor = Color.White;
                txb_path.BackColor = Color.FromArgb(20,20,20);
                txb_path.ForeColor = Color.White;
                lbl_userm.BackColor = Color.FromArgb(20, 20, 20);
                lbl_userm.ForeColor = Color.White;
                txb_list.BackColor = Color.FromArgb(20, 20, 20);
                txb_list.ForeColor = Color.White;
                main_panel.BackColor = Color.FromArgb(20, 20, 20);
                mods_toggle.Theme = MetroFramework.MetroThemeStyle.Dark;
                lbl_mod.BackColor = Color.FromArgb(20, 20, 20);
                lbl_mod.ForeColor = Color.White;

            }
        }

        private void genBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string res = null;
                m.testController(ref res, ref mod);
                //MessageBox.Show(res, "Mod Loading", MessageBoxButtons.OK, MessageBoxIcon.Information);
                MetroFramework.MetroMessageBox.Show(this, res, "Mod Loading", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                string[] tmp = m.getJSON().ToArray();
                txb_list.Lines = tmp;
                if (mod == true)
                {
                    mods_toggle.Checked = true;
                }
                else
                {
                    mods_toggle.Checked = false;
                }
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Mods file not found.","Mod Load", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        // Mod detec
        private void mods_toggle_CheckedChanged(object sender, EventArgs e)
        {
            // No Mods
            if (mod_flag == false)
            {
                mod_flag = true;

            }
            // Modded
            else
            {
                mod_flag = false;
            }
        }
    }
}
