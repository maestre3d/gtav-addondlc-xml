﻿namespace AddonX
{
    partial class root_menu
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(root_menu));
            this.root_panel = new System.Windows.Forms.Panel();
            this.main_panel = new System.Windows.Forms.Panel();
            this.bottom_panel = new System.Windows.Forms.Panel();
            this.side_panel = new System.Windows.Forms.Panel();
            this.icon_save = new System.Windows.Forms.Label();
            this.saveBtn = new MetroFramework.Controls.MetroButton();
            this.icon_folder = new System.Windows.Forms.Label();
            this.openBtn = new MetroFramework.Controls.MetroButton();
            this.addon_logo = new System.Windows.Forms.PictureBox();
            this.win_panel = new System.Windows.Forms.Panel();
            this.win_min = new System.Windows.Forms.Label();
            this.win_max = new System.Windows.Forms.Label();
            this.win_exit = new System.Windows.Forms.Label();
            this.win_Addon = new System.Windows.Forms.Label();
            this.lbl_path = new System.Windows.Forms.Label();
            this.txb_path = new System.Windows.Forms.TextBox();
            this.genBtn = new MetroFramework.Controls.MetroButton();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_userm = new System.Windows.Forms.Label();
            this.metroToolTip1 = new MetroFramework.Components.MetroToolTip();
            this.txb_list = new System.Windows.Forms.TextBox();
            this.clrBtn = new MetroFramework.Controls.MetroButton();
            this.mods_toggle = new MetroFramework.Controls.MetroToggle();
            this.lbl_mod = new System.Windows.Forms.Label();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.root_panel.SuspendLayout();
            this.main_panel.SuspendLayout();
            this.bottom_panel.SuspendLayout();
            this.side_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.addon_logo)).BeginInit();
            this.win_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // root_panel
            // 
            this.root_panel.Controls.Add(this.main_panel);
            this.root_panel.Controls.Add(this.bottom_panel);
            this.root_panel.Controls.Add(this.side_panel);
            this.root_panel.Controls.Add(this.win_panel);
            this.root_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.root_panel.Location = new System.Drawing.Point(0, 0);
            this.root_panel.Name = "root_panel";
            this.root_panel.Size = new System.Drawing.Size(854, 541);
            this.root_panel.TabIndex = 0;
            // 
            // main_panel
            // 
            this.main_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.main_panel.Controls.Add(this.mods_toggle);
            this.main_panel.Controls.Add(this.txb_list);
            this.main_panel.Controls.Add(this.txb_path);
            this.main_panel.Controls.Add(this.lbl_userm);
            this.main_panel.Controls.Add(this.lbl_mod);
            this.main_panel.Controls.Add(this.lbl_path);
            this.main_panel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.main_panel.Location = new System.Drawing.Point(167, 24);
            this.main_panel.Name = "main_panel";
            this.main_panel.Size = new System.Drawing.Size(687, 457);
            this.main_panel.TabIndex = 3;
            // 
            // bottom_panel
            // 
            this.bottom_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.bottom_panel.Controls.Add(this.metroButton1);
            this.bottom_panel.Controls.Add(this.genBtn);
            this.bottom_panel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottom_panel.Location = new System.Drawing.Point(167, 481);
            this.bottom_panel.Name = "bottom_panel";
            this.bottom_panel.Size = new System.Drawing.Size(687, 60);
            this.bottom_panel.TabIndex = 2;
            // 
            // side_panel
            // 
            this.side_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(123)))), ((int)(((byte)(138)))));
            this.side_panel.Controls.Add(this.label2);
            this.side_panel.Controls.Add(this.icon_save);
            this.side_panel.Controls.Add(this.clrBtn);
            this.side_panel.Controls.Add(this.saveBtn);
            this.side_panel.Controls.Add(this.icon_folder);
            this.side_panel.Controls.Add(this.openBtn);
            this.side_panel.Controls.Add(this.addon_logo);
            this.side_panel.Dock = System.Windows.Forms.DockStyle.Left;
            this.side_panel.Location = new System.Drawing.Point(0, 24);
            this.side_panel.Name = "side_panel";
            this.side_panel.Size = new System.Drawing.Size(167, 517);
            this.side_panel.TabIndex = 1;
            // 
            // icon_save
            // 
            this.icon_save.AutoSize = true;
            this.icon_save.BackColor = System.Drawing.Color.Transparent;
            this.icon_save.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.icon_save.Font = new System.Drawing.Font("Font Awesome 5 Free Solid", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.icon_save.ForeColor = System.Drawing.Color.White;
            this.icon_save.Location = new System.Drawing.Point(12, 207);
            this.icon_save.Name = "icon_save";
            this.icon_save.Size = new System.Drawing.Size(33, 25);
            this.icon_save.TabIndex = 4;
            this.icon_save.Text = "save";
            // 
            // saveBtn
            // 
            this.saveBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.saveBtn.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.saveBtn.ForeColor = System.Drawing.Color.White;
            this.saveBtn.Location = new System.Drawing.Point(-1, 195);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(168, 49);
            this.saveBtn.Style = MetroFramework.MetroColorStyle.Teal;
            this.saveBtn.TabIndex = 6;
            this.saveBtn.Text = "Save     ";
            this.saveBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.saveBtn.UseCustomBackColor = true;
            this.saveBtn.UseCustomForeColor = true;
            this.saveBtn.UseSelectable = true;
            this.saveBtn.UseStyleColors = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            this.saveBtn.MouseHover += new System.EventHandler(this.saveBtn_MouseHover);
            // 
            // icon_folder
            // 
            this.icon_folder.AutoSize = true;
            this.icon_folder.BackColor = System.Drawing.Color.Transparent;
            this.icon_folder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.icon_folder.Font = new System.Drawing.Font("Font Awesome 5 Free Solid", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.icon_folder.ForeColor = System.Drawing.Color.White;
            this.icon_folder.Location = new System.Drawing.Point(12, 158);
            this.icon_folder.Name = "icon_folder";
            this.icon_folder.Size = new System.Drawing.Size(39, 25);
            this.icon_folder.TabIndex = 3;
            this.icon_folder.Text = "folder-open";
            // 
            // openBtn
            // 
            this.openBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.openBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.openBtn.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.openBtn.ForeColor = System.Drawing.Color.White;
            this.openBtn.Location = new System.Drawing.Point(-1, 146);
            this.openBtn.Name = "openBtn";
            this.openBtn.Size = new System.Drawing.Size(168, 49);
            this.openBtn.Style = MetroFramework.MetroColorStyle.Teal;
            this.openBtn.TabIndex = 5;
            this.openBtn.Text = "Open    ";
            this.openBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.openBtn.UseCustomBackColor = true;
            this.openBtn.UseCustomForeColor = true;
            this.openBtn.UseSelectable = true;
            this.openBtn.UseStyleColors = true;
            this.openBtn.Click += new System.EventHandler(this.openBtn_Click);
            this.openBtn.MouseHover += new System.EventHandler(this.openBtn_MouseHover);
            // 
            // addon_logo
            // 
            this.addon_logo.Image = ((System.Drawing.Image)(resources.GetObject("addon_logo.Image")));
            this.addon_logo.Location = new System.Drawing.Point(12, 6);
            this.addon_logo.Name = "addon_logo";
            this.addon_logo.Size = new System.Drawing.Size(132, 115);
            this.addon_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.addon_logo.TabIndex = 0;
            this.addon_logo.TabStop = false;
            // 
            // win_panel
            // 
            this.win_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(130)))), ((int)(((byte)(112)))));
            this.win_panel.Controls.Add(this.win_min);
            this.win_panel.Controls.Add(this.win_max);
            this.win_panel.Controls.Add(this.win_exit);
            this.win_panel.Controls.Add(this.win_Addon);
            this.win_panel.Dock = System.Windows.Forms.DockStyle.Top;
            this.win_panel.Location = new System.Drawing.Point(0, 0);
            this.win_panel.Name = "win_panel";
            this.win_panel.Size = new System.Drawing.Size(854, 24);
            this.win_panel.TabIndex = 0;
            this.win_panel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.win_panel_MouseDown);
            this.win_panel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.win_panel_MouseMove);
            // 
            // win_min
            // 
            this.win_min.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.win_min.AutoSize = true;
            this.win_min.Cursor = System.Windows.Forms.Cursors.Hand;
            this.win_min.Font = new System.Drawing.Font("Font Awesome 5 Free Solid", 12F, System.Drawing.FontStyle.Bold);
            this.win_min.ForeColor = System.Drawing.Color.Green;
            this.win_min.Location = new System.Drawing.Point(767, 4);
            this.win_min.Name = "win_min";
            this.win_min.Size = new System.Drawing.Size(24, 17);
            this.win_min.TabIndex = 1;
            this.win_min.Text = "circle";
            this.win_min.Click += new System.EventHandler(this.win_min_Click);
            // 
            // win_max
            // 
            this.win_max.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.win_max.AutoSize = true;
            this.win_max.Cursor = System.Windows.Forms.Cursors.Hand;
            this.win_max.Font = new System.Drawing.Font("Font Awesome 5 Free Solid", 12F, System.Drawing.FontStyle.Bold);
            this.win_max.ForeColor = System.Drawing.Color.Gold;
            this.win_max.Location = new System.Drawing.Point(797, 4);
            this.win_max.Name = "win_max";
            this.win_max.Size = new System.Drawing.Size(24, 17);
            this.win_max.TabIndex = 1;
            this.win_max.Text = "circle";
            this.win_max.Click += new System.EventHandler(this.win_max_Click);
            // 
            // win_exit
            // 
            this.win_exit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.win_exit.AutoSize = true;
            this.win_exit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.win_exit.Font = new System.Drawing.Font("Font Awesome 5 Free Solid", 12F, System.Drawing.FontStyle.Bold);
            this.win_exit.ForeColor = System.Drawing.Color.Red;
            this.win_exit.Location = new System.Drawing.Point(827, 4);
            this.win_exit.Name = "win_exit";
            this.win_exit.Size = new System.Drawing.Size(24, 17);
            this.win_exit.TabIndex = 1;
            this.win_exit.Text = "circle";
            this.win_exit.Click += new System.EventHandler(this.exit_Click);
            // 
            // win_Addon
            // 
            this.win_Addon.AutoSize = true;
            this.win_Addon.Font = new System.Drawing.Font("AvenirNext LT Pro Regular", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.win_Addon.ForeColor = System.Drawing.Color.White;
            this.win_Addon.Location = new System.Drawing.Point(12, 3);
            this.win_Addon.Name = "win_Addon";
            this.win_Addon.Size = new System.Drawing.Size(158, 18);
            this.win_Addon.TabIndex = 0;
            this.win_Addon.Text = "AddonX For GTA V ™";
            // 
            // lbl_path
            // 
            this.lbl_path.AutoSize = true;
            this.lbl_path.Font = new System.Drawing.Font("AvenirNext LT Pro Regular", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_path.ForeColor = System.Drawing.Color.White;
            this.lbl_path.Location = new System.Drawing.Point(82, 52);
            this.lbl_path.Name = "lbl_path";
            this.lbl_path.Size = new System.Drawing.Size(47, 23);
            this.lbl_path.TabIndex = 0;
            this.lbl_path.Text = "Path";
            // 
            // txb_path
            // 
            this.txb_path.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.txb_path.Font = new System.Drawing.Font("Segoe UI Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txb_path.ForeColor = System.Drawing.Color.White;
            this.txb_path.Location = new System.Drawing.Point(86, 78);
            this.txb_path.Name = "txb_path";
            this.txb_path.ReadOnly = true;
            this.txb_path.Size = new System.Drawing.Size(429, 25);
            this.txb_path.TabIndex = 1;
            this.txb_path.Text = "Grand Theft Auto V Directory";
            // 
            // genBtn
            // 
            this.genBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.genBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.genBtn.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.genBtn.ForeColor = System.Drawing.Color.White;
            this.genBtn.Highlight = true;
            this.genBtn.Location = new System.Drawing.Point(0, 0);
            this.genBtn.Name = "genBtn";
            this.genBtn.Size = new System.Drawing.Size(150, 60);
            this.genBtn.Style = MetroFramework.MetroColorStyle.Teal;
            this.genBtn.TabIndex = 6;
            this.genBtn.Text = "Get Mods";
            this.genBtn.Theme = MetroFramework.MetroThemeStyle.Light;
            this.genBtn.UseCustomBackColor = true;
            this.genBtn.UseCustomForeColor = true;
            this.genBtn.UseSelectable = true;
            this.genBtn.UseStyleColors = true;
            this.genBtn.Click += new System.EventHandler(this.genBtn_Click);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("AvenirNext LT Pro Regular", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(23, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 18);
            this.label2.TabIndex = 7;
            this.label2.Text = "Directory Tools";
            // 
            // lbl_userm
            // 
            this.lbl_userm.AutoSize = true;
            this.lbl_userm.Font = new System.Drawing.Font("AvenirNext LT Pro Regular", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_userm.ForeColor = System.Drawing.Color.White;
            this.lbl_userm.Location = new System.Drawing.Point(82, 119);
            this.lbl_userm.Name = "lbl_userm";
            this.lbl_userm.Size = new System.Drawing.Size(116, 23);
            this.lbl_userm.TabIndex = 0;
            this.lbl_userm.Text = "User\'s mods";
            // 
            // metroToolTip1
            // 
            this.metroToolTip1.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroToolTip1.StyleManager = null;
            this.metroToolTip1.Theme = MetroFramework.MetroThemeStyle.Dark;
            // 
            // txb_list
            // 
            this.txb_list.AcceptsReturn = true;
            this.txb_list.AcceptsTab = true;
            this.txb_list.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txb_list.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(20)))), ((int)(((byte)(20)))), ((int)(((byte)(20)))));
            this.txb_list.Font = new System.Drawing.Font("AvenirNext LT Pro Regular", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txb_list.ForeColor = System.Drawing.Color.White;
            this.txb_list.Location = new System.Drawing.Point(86, 146);
            this.txb_list.Multiline = true;
            this.txb_list.Name = "txb_list";
            this.txb_list.ReadOnly = true;
            this.txb_list.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txb_list.Size = new System.Drawing.Size(492, 287);
            this.txb_list.TabIndex = 2;
            // 
            // clrBtn
            // 
            this.clrBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.clrBtn.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.clrBtn.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.clrBtn.ForeColor = System.Drawing.Color.White;
            this.clrBtn.Location = new System.Drawing.Point(0, 468);
            this.clrBtn.Name = "clrBtn";
            this.clrBtn.Size = new System.Drawing.Size(168, 49);
            this.clrBtn.Style = MetroFramework.MetroColorStyle.Teal;
            this.clrBtn.TabIndex = 6;
            this.clrBtn.Text = "Light Mode";
            this.clrBtn.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.clrBtn.UseCustomBackColor = true;
            this.clrBtn.UseCustomForeColor = true;
            this.clrBtn.UseSelectable = true;
            this.clrBtn.UseStyleColors = true;
            this.clrBtn.Click += new System.EventHandler(this.clrBtn_Click);
            this.clrBtn.MouseHover += new System.EventHandler(this.saveBtn_MouseHover);
            // 
            // mods_toggle
            // 
            this.mods_toggle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mods_toggle.AutoSize = true;
            this.mods_toggle.Enabled = false;
            this.mods_toggle.Location = new System.Drawing.Point(537, 86);
            this.mods_toggle.Name = "mods_toggle";
            this.mods_toggle.Size = new System.Drawing.Size(80, 17);
            this.mods_toggle.Style = MetroFramework.MetroColorStyle.Teal;
            this.mods_toggle.TabIndex = 3;
            this.mods_toggle.Text = "Off";
            this.mods_toggle.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.mods_toggle.UseSelectable = true;
            this.mods_toggle.CheckedChanged += new System.EventHandler(this.mods_toggle_CheckedChanged);
            // 
            // lbl_mod
            // 
            this.lbl_mod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lbl_mod.AutoSize = true;
            this.lbl_mod.Font = new System.Drawing.Font("AvenirNext LT Pro Regular", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_mod.ForeColor = System.Drawing.Color.White;
            this.lbl_mod.Location = new System.Drawing.Point(534, 56);
            this.lbl_mod.Name = "lbl_mod";
            this.lbl_mod.Size = new System.Drawing.Size(95, 18);
            this.lbl_mod.TabIndex = 0;
            this.lbl_mod.Text = "Mods Folder";
            // 
            // metroButton1
            // 
            this.metroButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.metroButton1.FontSize = MetroFramework.MetroButtonSize.Tall;
            this.metroButton1.FontWeight = MetroFramework.MetroButtonWeight.Light;
            this.metroButton1.ForeColor = System.Drawing.Color.White;
            this.metroButton1.Highlight = true;
            this.metroButton1.Location = new System.Drawing.Point(537, 0);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(150, 60);
            this.metroButton1.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroButton1.TabIndex = 6;
            this.metroButton1.Text = "Save DLCList";
            this.metroButton1.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroButton1.UseCustomBackColor = true;
            this.metroButton1.UseCustomForeColor = true;
            this.metroButton1.UseSelectable = true;
            this.metroButton1.UseStyleColors = true;
            this.metroButton1.Click += new System.EventHandler(this.genBtn_Click);
            // 
            // root_menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 541);
            this.Controls.Add(this.root_panel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "root_menu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AddonX";
            this.root_panel.ResumeLayout(false);
            this.main_panel.ResumeLayout(false);
            this.main_panel.PerformLayout();
            this.bottom_panel.ResumeLayout(false);
            this.side_panel.ResumeLayout(false);
            this.side_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.addon_logo)).EndInit();
            this.win_panel.ResumeLayout(false);
            this.win_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel root_panel;
        private System.Windows.Forms.Panel side_panel;
        private System.Windows.Forms.Panel win_panel;
        private System.Windows.Forms.Panel main_panel;
        private System.Windows.Forms.PictureBox addon_logo;
        private System.Windows.Forms.Label win_Addon;
        private System.Windows.Forms.Label win_min;
        private System.Windows.Forms.Label win_max;
        private System.Windows.Forms.Label win_exit;
        private System.Windows.Forms.Label icon_save;
        private System.Windows.Forms.Label icon_folder;
        private MetroFramework.Controls.MetroButton openBtn;
        private MetroFramework.Controls.MetroButton saveBtn;
        private System.Windows.Forms.Panel bottom_panel;
        private System.Windows.Forms.Label lbl_path;
        private System.Windows.Forms.TextBox txb_path;
        private MetroFramework.Controls.MetroButton genBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_userm;
        private MetroFramework.Components.MetroToolTip metroToolTip1;
        private System.Windows.Forms.TextBox txb_list;
        private MetroFramework.Controls.MetroButton clrBtn;
        private MetroFramework.Controls.MetroToggle mods_toggle;
        private System.Windows.Forms.Label lbl_mod;
        private MetroFramework.Controls.MetroButton metroButton1;
    }
}

