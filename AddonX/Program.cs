﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace AddonX
{
    static class Program
    {
        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Thread t1 = new Thread(new ThreadStart(initBoot));
            t1.Name = "Boot Launcher";
            Thread t2 = new Thread(new ThreadStart(initMenu));
            t2.Name = "Main Menu";
            t2.SetApartmentState(ApartmentState.STA);

            t1.IsBackground = true;

            t1.Start();
            Thread.Sleep(4000);
            t1.Abort();
            t2.Start();
        }
        static void initBoot()
        {
            Application.Run(new boot_launcher());
        }

        static void initMenu()
        {
            Application.Run(new root_menu());
        }

    }
}
