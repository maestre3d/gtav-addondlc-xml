﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Newtonsoft.Json;
using System.IO;

namespace Controllers
{
    public sealed class modsController
    {
        Mods mods;
        pathController pa = new pathController();
        static string x;

        public void testController(ref string res, ref bool flag)
        {
            
            mods = new Mods();
            mods.user_mods = new List<string> { };

            try
            {
                x = pa.getPathJSON();
                if (Directory.Exists(x + @"\mods\update\x64\dlcpacks"))
                {
                    foreach (var d in System.IO.Directory.GetDirectories(x + @"\update\x64\dlcpacks"))
                    {
                        var dirName = new DirectoryInfo(d).Name;
                        mods.user_mods.Add(dirName);
                    }
                    foreach (var d in System.IO.Directory.GetDirectories(x + @"\mods\update\x64\dlcpacks"))
                    {
                        var dirName = new DirectoryInfo(d).Name;
                        mods.user_mods.Add(dirName);
                    }
                    flag = true;
                }
                else
                {
                    foreach (var d in System.IO.Directory.GetDirectories(x + @"\update\x64\dlcpacks"))
                    {
                        var dirName = new DirectoryInfo(d).Name;
                        mods.user_mods.Add(dirName);
                    }
                    flag = false;
                }
                string path_save = System.IO.Path.Combine(Environment.GetFolderPath(
                Environment.SpecialFolder.MyDoc‌​uments), "Rockstar Games\\GTA V\\AddonX", "mods.json");

                // string.Join(", ", mods.user_mods.ToArray())

                // serialize JSON to a string and then write string to a file
                File.WriteAllText(path_save, JsonConvert.SerializeObject(mods));
                // serialize JSON directly to a file
                using (StreamWriter file = File.CreateText(path_save))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    serializer.Serialize(file, mods);
                    res = "Your mods are loaded.";
                }
            }
            catch(DirectoryNotFoundException)
            {
                res = "Directory Not Found";

            }
        }

        public List<string> getJSON()
        {
            List<string> x;
            try
            {
                string path_save = System.IO.Path.Combine(Environment.GetFolderPath(
                Environment.SpecialFolder.MyDoc‌​uments), "Rockstar Games\\GTA V\\AddonX", "mods.json");
                // read file into a string and deserialize JSON to a type
                Mods caca = JsonConvert.DeserializeObject<Mods>(File.ReadAllText(path_save));
                x = (caca.user_mods);
                return x;
            }
            catch (FileNotFoundException)
            {
                return null;
            }
            catch (DirectoryNotFoundException)
            {
                return null;
            }
        }
    }
}
