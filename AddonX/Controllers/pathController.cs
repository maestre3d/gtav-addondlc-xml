﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Models;
using Newtonsoft.Json;
using System.IO;

namespace Controllers
{
    public class pathController
    {
        Models.Path path;
        private bool created_path = false;

        public void savePath(ref string message)
        {
            try
            {
                if (path != null)
                {
                    string path_new = System.IO.Path.Combine(Environment.GetFolderPath(
                    Environment.SpecialFolder.MyDoc‌​uments), "Rockstar Games\\GTA V\\AddonX");

                    string path_save = System.IO.Path.Combine(Environment.GetFolderPath(
                    Environment.SpecialFolder.MyDoc‌​uments), "Rockstar Games\\GTA V\\AddonX", "path.json");

                    if (Directory.Exists(path_new))
                    {
                        // serialize JSON to a string and then write string to a file
                        File.WriteAllText(path_save, JsonConvert.SerializeObject(path));
                        // serialize JSON directly to a file
                        using (StreamWriter file = File.CreateText(path_save))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            serializer.Serialize(file, path);
                            if (created_path == false)
                            {
                                message = "Path has been saved.";
                            }
                            else
                            {
                                message = "AddonX folder created and path saved.\n(Stored at Documents/Rockstar Games/GTA V)";
                            }
                        }
                    }
                    else
                    {
                        created_path = true;
                        System.IO.Directory.CreateDirectory(path_new);
                        message = null;
                        savePath(ref message);
                    }
                }
                else
                {
                    message = "Directory is not added.";
                }


            }
            catch ( DirectoryNotFoundException)
            {
                message = "Directory not found";
            }

        }

        public void setPath(string x)
        {
            path = new Models.Path { user_path = @""+x };
        }

        public string getPathJSON()
        {
            string x;
            try
            {
                string path_save = System.IO.Path.Combine(Environment.GetFolderPath(
                Environment.SpecialFolder.MyDoc‌​uments), "Rockstar Games\\GTA V\\AddonX", "path.json");
                // read file into a string and deserialize JSON to a type
                Models.Path caca = JsonConvert.DeserializeObject<Models.Path>(File.ReadAllText(path_save));
                x = caca.user_path;
                return x;
            }
            catch (FileNotFoundException)
            {
                return null;
            }
            catch(DirectoryNotFoundException)
            {
                return null;
            }
        }

    }
}
